<?php

namespace App\Http\Controllers\Backend;

use App\Models\Classroom;
use App\Models\Group;
use App\Models\Subject;
Use App\Models\Student;
/**
 * Class DashboardController.
 */
class DashboardController
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('backend.dashboard');
    }
    public function class(){
        $classrooms = Classroom::all();
        return view('backend.class',['classrooms'=>$classrooms]);
    }
    public function subject(){
        $subjects = Subject::all();
        return view('backend.subject',['subjects'=>$subjects]);
    }
    public function group(){
        $groups = Group::all();
        return view('backend.group',['groups'=>$groups]);
    }
    public function student(){
        $students = Student::all();
        
        return view('backend.student',['students'=>$students]);
    }
    public function create_class(){
        return view('backend.create_class');
    }
}