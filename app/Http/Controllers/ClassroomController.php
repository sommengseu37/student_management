<?php

namespace App\Http\Controllers;

use App\Domains\Auth\Http\Requests\Backend\User\UpdateUserRequest;
// use App\Http\Requests\Frontend\User\UpdateClassroomRequest as UserUpdateClassroomRequest;
use App\Models\Classroom;
use App\Http\Requests\Frontend\User\UpdateClassroomRequest;
use App\Http\Requests\CreateClassroomRequest;
use App\Http\Requests\EditClassroomRequest;
use App\Models\Subject;
use Illuminate\Http\Request;
use Torann\GeoIP\Console\Update;
@include('App\Models\Classroom');

use function PHPSTORM_META\registerArgumentsSet;

class ClassroomController extends Controller
{   
    public function index(){
        $classrooms = Classroom::all();
        return view('classroom.index', ['classrooms' => $classrooms]);
        // return view('classroom.index');
    }
    public function create(){
        return view('backend/classroom.create');
    }
    public function store(CreateClassroomRequest $request){
        Classroom::create([
            'subject_id' => $request->subject_id,
            'group_id' => $request->group_id,
            'start_at' => $request->start_at,
            'end_at' => $request->end_at,
        ]);
        return redirect('/admin/class')->with('success','Classroom has been create successfully!'); 
    }
    public function show($id){
        $classroom = classroom::where('id', $id)->first();
        return view('backend/classroom.show',['classrooms' => $classroom]);
    }
    public function edit($id){
        $classroom = classroom::where('id', $id)->first();
        return view('backend/classroom.edit',['classrooms' => $classroom]);
    }


    public function update($id,UpdateClassroomRequest $request){
        $classroom = classroom::find($id);
        $classroom->subject_id = $request->subject_id;
        $classroom->group_id = $request->group_id;
        $classroom->start_at = $request->start_at;
        $classroom->end_at = $request->end_at;
        $classroom->save();
        return redirect(route('backend/classroom.show',$classroom->id))->with('success', 'classroom has been updated');
    }
    public function delete($id){
        $classroom = classroom::find($id);
        $classroom->delete();
        return redirect('/admin/class');
    }
}
