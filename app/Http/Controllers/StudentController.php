<?php

namespace App\Http\Controllers;

use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\CreateStudentRequest;

use Redirect;

class StudentController extends Controller
{
    public function index(){
        $students = Student::all();
            
        return view("backend/student" ,["students" => $students]);
        
    }
    public function create(){
        return view ("backend/Student.create");
    }
    public function store(CreateStudentRequest $request){
        // Student::create([
        //     'first_name' => $request->first_name,
        //     'last_name' => $request->last_name,
        //     'dob' => $request->dob,
        //     'email' => $request->email,
        //     'address' => $request->address,
        //     'phone' => $request->phone,
        //     'photo' => $request->photo,

        //     if( $request->photo)
        // ]);
        // $file = $request->file("photo")->store("storage/upload", "custom");
            $data = array(
                'first_name' => $request->first_name,
                'last_name' => $request->last_name,
                'dob' => $request->dob,
                'email' => $request->email,
                'address' => $request->address,
                'phone' => $request->phone,
            );
            if($request->photo){
                $data["photo"] = $request->file('photo')->store("/upload", "custom");
            }
            $students = Student::create($data);

       return Redirect('admin/student')->with('success', 'Classroom has been create successfully!',$students); 
    }
    // public function show(){
    //     return view('student.show');
    // }




    public function delete($id){
        $students = Student::find($id);
        $students->delete();
        return redirect('/admin/student');
    }

}
