<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Nullable;

class AddSomeColumnsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('gender',7)->nullable()->after('name');
            $table->string('address',100)->nullable()->after('email_verified_at');
            $table->string('dob')->nullable()->after('address');
            $table->string('salary')->nullable()->after('password');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
           $table->dropColumn('gender'); 
           $table->dropColumn('address'); 
           $table->dropColumn('dob'); 
           $table->dropColumn('salary'); 
        });
    }
}
