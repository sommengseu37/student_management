<?php

namespace Database\Seeders\Auth;

use App\Domains\Auth\Models\User;
use Database\Seeders\Traits\DisableForeignKeys;
use Illuminate\Database\Seeder;

/**
 * Class UserRoleTableSeeder.
 */
class UserRoleSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        User::find(1)->assignRole('Administrator');
        User::find(2)->assignRole('Manager');
        User::find(3)->assignRole('Teacher');
        User::find(4)->assignRole('Student');
        User::find(5)->assignRole('Cleaner');

        //teacher can view student list
        User::find(3)->syncPermissions([
            'admin.access.user.list'
        ]);
        $this->enableForeignKeys();
    }
}