<?php

namespace Database\Seeders\Auth;

use App\Domains\Auth\Models\User;
use Database\Seeders\Traits\DisableForeignKeys;
use Illuminate\Database\Seeder;

/**
 * Class UserTableSeeder.
 */
class UserSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Add the master administrator, user id of 1
        User::create([
            'id' => 1,
            'type' => User::TYPE_ADMIN,
            'name' => 'Super Admin',
            'email' => 'admin@admin.com',
            'password' => 'secret',
            'email_verified_at' => now(),
            'active' => true,
        ]);
        User::create([
            'id' => 2,
            'type' => User::TYPE_ADMIN,
            'name' => 'Sok',
            'email' => 'sok@manager.com',
            'password' => 'secret',
            'email_verified_at' => now(),
            'active' => true,
        ]);
        User::create([
            'id' => 3,
            'type' => User::TYPE_ADMIN,
            'name' => 'Saov',
            'email' => 'saov@teacher.com',
            'password' => 'saovteacher',
            'email_verified_at' => now(),
            'active' => true,
        ]);
        User::create([
            'id' => 4,
            'type' => User::TYPE_USER,
            'name' => 'Raksmey',
            'email' => 'raksmey@student.com',
            'password' => 'secret',
            'email_verified_at' => now(),
            'active' => true,
        ]);
        User::create([
            'id' => 5,
            'type' => User::TYPE_USER,
            'name' => 'Sokar',
            'email' => 'sokar@cleaner.com',
            'password' => 'secret',
            'email_verified_at' => now(),
            'active' => true,
        ]);

        if (app()->environment(['local', 'testing'])) {
            User::create([
                'type' => User::TYPE_USER,
                'name' => 'Test User',
                'email' => 'user@user.com',
                'password' => 'secret',
                'email_verified_at' => now(),
                'active' => true,
            ]);
        }
        
        $this->enableForeignKeys();
    }
}
