<?php

namespace Database\Seeders;
use App\Models\Classroom;
use Illuminate\Database\Seeder;
use Database\Seeders\Traits\TruncateTable;
class ClassroomSeeder extends Seeder
{
    use TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateMultiple([
            'Classrooms'
        ]);
        Classroom::create([
            'subject_id' => '1',
            'group_id' => '1',
            'start_at' => '2000',
            'end_at' => '2001'
        ]);
        Classroom::create([
            'subject_id' => '2',
            'group_id' => '1',
            'start_at' => '2000',
            'end_at' => '2001'
        ]);
        Classroom::create([
            'subject_id' => '3',
            'group_id' => '1',
            'start_at' => '2000',
            'end_at' => '2001'
        ]);
        Classroom::create([
            'subject_id' => '4',
            'group_id' => '1',
            'start_at' => '2000',
            'end_at' => '2001'
        ]);
    }
}
