<?php

namespace Database\Seeders;

use App\Models\Group;
use Illuminate\Database\Seeder;
use Database\Seeders\Traits\TruncateTable;

class GroupSeeder extends Seeder
{
    use TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateMultiple([
            'groups'
        ]);
        Group::create([
            'title' => 'IT01',
            'start_year' => '2000',
            'end_year' => '2001'
        ]);
        Group::create([
            'title' => 'IT02',
            'start_year' => '2001',
            'end_year' => '2002'
        ]);
        Group::create([
            'title' => 'IT03',
            'start_year' => '2002',
            'end_year' => '2003'
        ]);
        Group::create([
            'title' => 'IT04',
            'start_year' => '2003',
            'end_year' => '2004'
        ]);
        Group::create([
            'title' => 'IT05',
            'start_year' => '2004',
            'end_year' => '2005'
        ]);
    }
}
