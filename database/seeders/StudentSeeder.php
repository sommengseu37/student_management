<?php

namespace Database\Seeders;

// use public\photo;
use App\Models\Student;
use Illuminate\Database\Seeder;
use Database\Seeders\Traits\TruncateTable;

class StudentSeeder extends Seeder
{
    use TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateMultiple([
            'students'
        ]);
        Student::create([
            'photo' => 'photo\download.jpg',
            'first_name' => 'bro',
            'last_name' => 'hovbektnam',
            'dob' => '12.12.2002',
            'email' => 'hovbektnam@gmail.com',
            'phone' => '098765443',
            'address' => 'kpc',
        ]);
        Student::create([
            'photo' => 'photo\download.jpg',
            'first_name' => 'bro',
            'last_name' => 'hovbektnam',
            'dob' => '12.12.2002',
            'email' => 'hovbektnam@gmail.com',
            'phone' => '098765443',
            'address' => 'kpc',
        ]);
        Student::create([
            'photo' => 'photo\student\9szofPxo91S78brQ9UjQvvzd0WlwVzE5ZwiogiJe.jpg',
            'first_name' => 'bro',
            'last_name' => 'hovbektnam',
            'dob' => '12.12.2002',
            'email' => 'hovbektnam@gmail.com',
            'phone' => '098765443',
            'address' => 'kpc',
        ]);
        Student::create([
            'photo' => 'photo\LmEruS1Hl8kT2OFffbl80dP0fZOSC7bLHTVeSGpc.png',
            'first_name' => 'bro',
            'last_name' => 'hovbektnam',
            'dob' => '12.12.2002',
            'email' => 'hovbektnam@gmail.com',
            'phone' => '098765443',
            'address' => 'kpc',
        ]);
    }
}
