<?php

namespace Database\Seeders;

use App\Models\Subject;
use Illuminate\Database\Seeder;
use Database\Seeders\Traits\TruncateTable;

class SubjectSeeder extends Seeder
{
    use TruncateTable;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->truncateMultiple([
            'subjects'
        ]);
        
        Subject::create([
            "subject" => "C#",
        ]);
        Subject::create([
            "subject" => "ds",
        ]);
        Subject::create([
            "subject" => "network",
        ]);
        Subject::create([
            "subject" => "gd",
        ]);
        Subject::create([
            "subject" => "dsm",
        ]);

    }
}
