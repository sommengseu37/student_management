@extends('backend.layouts.app')

@section('title', __('Dashboard'))
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous"> --}}
@push('after-styles')
    <style>
        .card {
            background: #dde1e7 !important;
            width: 300px;
            display: flex !important;
            
            
        }
        /* .row{
         width: 100px !important;
        } */
        
    </style>
@endpush
@section('content')
{{-- @include('includes/partials.messages') --}}
{{-- <form method="post" action="/classroom/store" class="row g-3"> --}}
   <x-backend.card>
      <x-slot name="header">
         {{-- Alert Message --}}
      </x-slot>
      <x-slot name="body">
         <form class="row g-3" method="post" action="/classroom/store">
            @csrf
            <div class="col-md-6">
               <label for="inputEmail4" class="form-label">SUBJECT ID</label>
               <input type="number" class="form-control" name="subject_id" required>
            </div>
            <div class="col-md-6">
               <label for="inputPassword4" class="form-label">GROUP ID</label>
               <input type="number" class="form-control" name="group_id" required>
            </div>
            <div class="col-12">
               <label for="inputAddress" class="form-label">START AT</label>
                  <select class="form-select" aria-label="Default select example" name="start_at">
                     <option selected>Select Year</option>
                     <option value="2010">2010</option>
                     <option value="2011">2011</option>
                     <option value="2012">2012</option>
                     <option value="2013">2013</option>
                     <option value="2014">2014</option>
                     <option value="2015">2015</option>
                     <option value="2016">2016</option>
                     <option value="2017">2017</option>
                     <option value="2018">2018</option>
                     <option value="2019">2019</option>
                     <option value="2020">2020</option>
                     <option value="2021">2021</option>
                     <option value="2022">2022</option>
                     <option value="2023">2023</option>
                     <option value="2024">2024</option>
                     <option value="2025">2025</option>
                     <option value="2026">2026</option>
                     <option value="2027">2027</option>
                     <option value="2028">2028</option>
                     <option value="2029">2029</option>
                     <option value="2030">2030</option>
                  </select>
            </div>
            <div class="col-12">
               <label for="inputAddress2" class="form-label">END AT</label>
                  <select class="form-select" aria-label="Default select example" name="end_at">
                     <option selected>Select Year</option>
                     <option value="2010">2010</option>
                     <option value="2011">2011</option>
                     <option value="2012">2012</option>
                     <option value="2013">2013</option>
                     <option value="2014">2014</option>
                     <option value="2015">2015</option>
                     <option value="2016">2016</option>
                     <option value="2017">2017</option>
                     <option value="2018">2018</option>
                     <option value="2019">2019</option>
                     <option value="2020">2020</option>
                     <option value="2021">2021</option>
                     <option value="2022">2022</option>
                     <option value="2023">2023</option>
                     <option value="2024">2024</option>
                     <option value="2025">2025</option>
                     <option value="2026">2026</option>
                     <option value="2027">2027</option>
                     <option value="2028">2028</option>
                     <option value="2029">2029</option>
                     <option value="2030">2030</option>
                  </select>
            </div>
            <div class="col-12">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form>
      </x-slot>       
      <x-slot name="footer">
      </x-slot>
   </x-backend.card>
@include('backend.layouts.alert')      
@endsection