@php
   // dd($classrooms);
@endphp
@extends('backend.layouts.app')
@section('title', __('Dashboard'))
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous"> --}}
@push('after-styles')
<style>
   .card {
      background: #f4f4f4 !important;
   }
   
   </style>
@endpush
@section('content')
<x-backend.card>
   <x-slot name="header">
      @include('includes/partials.alert')
   </x-slot>
   <x-slot name="body">
      <form method="post" class="row g-3" action="{{route('classroom.update' ,$classrooms->id)}}">
         @csrf
         <div class="col-md-6">
               <label for="inputEmail4" class="form-label">SUBJECT ID</label>
               <input type="number" class="form-control" name="subject_id" value="{{$classrooms->subject_id}}">
            </div>
            <div class="col-md-6">
               <label for="inputPassword4" class="form-label">GROUP ID</label>
               <input type="number" class="form-control" name="group_id" value="{{$classrooms->group_id}}">
            </div>
            <div class="col-12">
               <label for="inputAddress" class="form-label">START AT</label>
                  <select class="form-select" aria-label="Default select example" name="start_at">
                     <option selected>{{$classrooms->start_at}}</option>
                     <option value="2010">2010</option>
                     <option value="2011">2011</option>
                     <option value="2012">2012</option>
                     <option value="2013">2013</option>
                     <option value="2014">2014</option>
                     <option value="2015">2015</option>
                     <option value="2016">2016</option>
                     <option value="2017">2017</option>
                     <option value="2018">2018</option>
                     <option value="2019">2019</option>
                     <option value="2020">2020</option>
                     <option value="2021">2021</option>
                     <option value="2022">2022</option>
                     <option value="2023">2023</option>
                     <option value="2024">2024</option>
                     <option value="2025">2025</option>
                     <option value="2026">2026</option>
                     <option value="2027">2027</option>
                     <option value="2028">2028</option>
                     <option value="2029">2029</option>
                     <option value="2030">2030</option>
                  </select>
            </div>
            <div class="col-12">
               <label for="inputAddress2" class="form-label">END AT</label>
                  <select class="form-select" aria-label="Default select example" name="end_at">
                     <option selected>{{$classrooms->end_at}}</option>
                     <option value="2010">2010</option>
                     <option value="2011">2011</option>
                     <option value="2012">2012</option>
                     <option value="2013">2013</option>
                     <option value="2014">2014</option>
                     <option value="2015">2015</option>
                     <option value="2016">2016</option>
                     <option value="2017">2017</option>
                     <option value="2018">2018</option>
                     <option value="2019">2019</option>
                     <option value="2020">2020</option>
                     <option value="2021">2021</option>
                     <option value="2022">2022</option>
                     <option value="2023">2023</option>
                     <option value="2024">2024</option>
                     <option value="2025">2025</option>
                     <option value="2026">2026</option>
                     <option value="2027">2027</option>
                     <option value="2028">2028</option>
                     <option value="2029">2029</option>
                     <option value="2030">2030</option>
                  </select>
            </div>
            {{-- <div class="col-12">
               <button type="submit" class="btn btn-primary">Submit</button>
            </div>
         </form> --}}
      </x-slot>       
      <x-slot name="footer">
         <button type="success" class="btn btn-dark">submit</button>
         {{-- <button type="button" class="btn btn-dark">Back</button> --}}
         <a href="{{url('/admin/class')}}"><button type="button" class="btn btn-secondary">Cancel</button></a>
      </x-slot>
   </x-backend.card>
</form>       
@endsection