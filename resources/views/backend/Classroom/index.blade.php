@extends('backend.layouts.app')

@section('title', __('Dashboard'))
@push('after-styles')
    <style>
        .card {
            background: #fafafa !important; 
        }
        .table {
            height: 80px;
        }
      
    </style>
@endpush
@section('content')
    <x-backend.card>
        <x-slot name="header">
           
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>
        <x-slot name="body">
            @lang('')
            <table class="table table-hover">
                <thead>
                 <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Subject id</th>
                    <th scope="col">Group id</th>
                    <th scope="col">Start at</th>
                    <th scope="col">End at</th>
                    <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($classrooms as $item)   
                    <tr>
                    <th scope="row">{{$item->id}}</th>
                    <td>{{$item->subject_id}}</td>
                    <td>{{$item->group_id}}</td>
                    <td>{{$item->start_at}}</td>
                    <td>{{$item->end_at}}</td>
             
                    @endforeach
                </tbody>
            </table>
        </x-slot>
        <x-slot name="footer">
        </x-slot>
    </x-backend.card>
@endsection
