{{-- @php
   dd($classrooms);
@endphp --}}
@extends('backend.layouts.app')
@section('title', __('Dashboard'))
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous"> --}}
@push('after-styles')
<style>
   .card {
      background: #dde1e7!important;
   }
   
   
   </style>
@endpush
@section('content')
@include('includes/partials.alert')
<x-backend.card>
   <x-slot name="header">
      </x-slot>
      <x-slot name="body">
         <table class="table table-bordered">
            <thead>
               <tr>
                  <th scope="col">#</th>
                  <th scope="col">Subject_id</th>
                  <th scope="col">Group_id</th>
                  <th scope="col">Start at</th>
                  <th scope="col">End at</th>
               </tr>
            </thead>
            <tbody class="table-group-divider">
               <tr>
                  <th scope="row">{{$classrooms->id}}</th>
                  <td>{{$classrooms->subject_id}}</td>
                  <td>{{$classrooms->group_id}}</td>
                  <td>{{$classrooms->start_at}}</td>
                  <td>{{$classrooms->end_at}}</td>
               </tr>
            </tbody>
         </table>
      </x-slot>       
      <x-slot name="footer">
         <a href="{{'/admin/class'}}"><button type="submit" class="btn btn-dark">Back</button></a>
      </x-slot>
   </x-backend.card>
@endsection