@php
//    dd($students)
@endphp
@extends('backend.layouts.app')

@section('title', __('Dashboard'))
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous"> --}}
@push('after-styles')
    <style>
        .card {
            background: #dde1e7!important; 
            width: 700px!important;
            height: 52rem;
        }
        table{
            height: 200px !important;
        }
.image-preview-container {
    width: 5cm;
    height: 6cm;
    margin: 0 auto;
    border: 1px solid rgba(0, 0, 0, 0.1);
    padding:10px;
    /* border-radius: 20px; */
}

.image-preview-container img {
    width: 100%;
    height: auto;
    display: none;
    /* margin-bottom: 30px; */
}
.image-preview-container input {
    display: none;
    /* top: -100px; */
    position: relative;
}
/* 
.image-preview-container label {
    display: block;
    width: 45%;
    height: 45px;
    margin-left: 25%;
    text-align: center;
    background: #180d28;
    color: #fff;
    font-size: 15px;
    text-transform: Uppercase;
    font-weight: 400;
    border-radius: 5px;
    cursor: pointer;
    display: flex;
    align-items: center;
    justify-content: center; */
/* } */
    </style>
@endpush
@section('content')
{{-- @include('includes/partials.messages') --}}
{{-- <form method="post" action="/classroom/store" class="row g-3"> --}}
   <x-backend.card>
      <x-slot name="header">
      </x-slot>
      <x-slot name="body">
         <form class="row g-3" action="/student/store" method="post" enctype="multipart/form-data">
            @csrf
         <div class="col-md-6">
            <label for="inputEmail4" class="form-label" >First Name</label>
            <input type="text" class="form-control" placeholder="First name" name="first_name" required>
         </div>
         <div class="col-md-6">
            <label for="inputPassword4" class="form-label">Last Name</label>
            <input type="text" class="form-control" placeholder="Last name" name="last_name" required>
         </div>
         <div class="col-12">
            <label for="inputAddress" class="form-label">Date of birth</label>
            <input type="date" name="dob" class="form-control" placeholder="dd-mm-yyyy" value="" min="1997-01-01" max="2030-12-31" required>
         </div>
         <div class="col-12">
            <label for="inputEmail4" class="form-label">Email</label>
            <input type="email" class="form-control" placeholder="Email" name="email" required>
         </div>
         <div class="col-12">
            <label for="inputAddress2" class="form-label">Address</label>
            <input type="text" class="form-control" placeholder="Address" name="address" required>
         </div>
         <div class="col-md-6">
            <label for="inputCity" class="form-label">Phone Number</label>
            <input type="tel" class="form-control" placeholder="Phone number" name="phone" required>
         </div>
            <div class="col-12">
                  <picture>
                     <body>
                        <div class="image-preview-container">
                           <div class="preview">
                              <img class="img-thumbnail" id="preview-selected-image">
                           </div>
                        </div>
                  </picture>
            </div>
               <div class="col-md-6">
                  <input type="file" id="file-upload"  onchange="previewImage(event);" name="photo"/>
               </div>
         <div class="col-12">
            <button type="submit" class="btn btn-primary">Submit</button>
         </div>
         </form>
      </x-slot>       
      <x-slot name="footer">
      </x-slot>
   </x-backend.card>

@endsection