@php
    // dd($classrooms);
@endphp
@extends('backend.layouts.app')

@section('title', __('Dashboard'))
@push('after-styles')
    <style>
        .card {
            background: #dde1e7!important; 
        }
        
    </style>
@endpush
@section('content')
@include('includes/partials.alert')
    <x-backend.card>
        <x-slot name="header">
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>
        <x-slot name="body">
            @lang('')
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Subject id</th>
                        <th scope="col">Group id</th>
                        <th scope="col">Start at</th>
                        <th scope="col">End at</th>
                        <th scope="col">Created at</th>
                        <th scope="col">Updated at</th>
                        <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($classrooms as $item)   
                    <tr>
                        <td scope="row">{{$item->id}}</td>
                        <td scope="row">{{$item->subject_id}}</td>
                        <td scope="row">{{$item->group_id}}</td>
                        <td scope="row">{{$item->start_at}}</td>
                        <td scope="row">{{$item->end_at}}</td>
                        <td scope="row">{{$item->created_at}}</td>
                        <td scope="row">{{$item->updated_at}}</td>
                        <td>
                            <a href="{{route('classroom.show',$item->id)}}" type="button">
                                <button type="button" class="btn btn-success"">
                                    <i class="bi bi-eye"></i>
                                </button>
                            </a>
                            <a href="{{route('classroom.edit',$item->id)}}" type="button">
                                <button type="button" class="btn btn-primary">
                                    <i class="bi bi-pencil-square"></i>
                                </button>
                            </a>
                            {{-- <a href="{{route('',$item->id)}}" type="button"> --}}
                                <button onClick="delete_rec({{$item->id}})" type="button" class="btn btn-danger">
                                    <i class="bi bi-trash"></i>
                                </button>
                                {{-- </a> --}}
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </x-slot>
            <x-slot name="footer">
                <a href="{{route('classroom.create')}}">
                    <button type="button" class="btn btn-dark">
                        <i class="bi bi-plus-square"></i>
                    </button>
                </a>
            </x-slot>
        </x-backend.card>
        @endsection
        