@extends('backend.layouts.app')

@section('title', __('Dashboard'))
@push('after-styles')
    <style>
        .card {
            background: #dde1e7 !important;
        }
    </style>
@endpush
@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>

        <x-slot name="body">
            @lang('Welcome to the Dashboard')
        </x-slot>
    </x-backend.card>
@endsection
