@php
    // dd($groups)
@endphp
@extends('backend.layouts.app')

@section('title', __('Dashboard'))
@push('after-styles')
    <style>
        .card {
            background: #fdfbfb !important;
        }
    </style>
@endpush
@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>
            
        <x-slot name="body">
            @lang('')
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Title</th>
                        <th scope="col">Start Year</th>
                        <th scope="col">End Year</th>
                    </tr>
                </thead>
                @foreach ($groups as $item)
                    <tr>
                        <th scope="row">{{$item->id}}</th>

                        <td>{{$item->title}}</td>
                        <td>{{$item->start_year}}</td>
                        <td>{{$item->end_year}}</td>
                    </tr>
                 @endforeach
            </table>
        </x-slot>
    </x-backend.card>
@endsection
