            <div class="container">  
                @if($errors->all())
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-primary" role="alert">
                            {{ $error }}
                        </div>
                    @endforeach
                @endif
                @if(session()->get('success'))
                    <div class="alert alert-success" role="alert">
                        {{ session()->get('success') }}
                        <i class="bi bi-check-circle"></i>
                    </div>
                @endif
                @yield('content')
            </div>