@php
//    dd($students);
@endphp
@extends('backend.layouts.app')
{{-- @extends('photo/student') --}}
@section('title', __('Dashboard'))
{{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous"> --}}
@push('after-styles')
    <style>
        .card {
            background: #dde1e7!important; 
        }
        table{
            width: 100%!important;
            height: 200px !important;
            /* background: red */
        }
    </style>
@endpush
@section('content')
{{-- @include('includes/partials.messages') --}}
{{-- <form method="post" action="/classroom/store" class="row g-3"> --}}
   <x-backend.card>
      <x-slot name="header">
      </x-slot>
      <x-slot name="body">

        <table class="table table-sm">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Photo</th>
                    <th scope="col">Fist Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">DOB</th>
                    <th scope="col">Email</th>
                    <th scope="col">Phone number</th>
                    <th scope="col">Address</th>
                    <th scope="col">Start At</th>
                    <th scope="col">End At</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
               @foreach($students as $item)
               <tr>
                   <td scope="row">#{{$item->id}}</td>
                   @php
                        // dd($item->photo);
                        @endphp
                    <td scope="row">
                         {{-- <img src="{{$item->photo}}" width="50px" height="50px">  --}}
                         <img src="/storage/{{$item->photo}}" width="50px" height="50px"> 
                    </td>
                    <td scope="row">{{$item->first_name}}</td>
                    <td scope="row">{{$item->last_name}}</td>
                    <td scope="row">{{$item->dob}}</td>
                    <td scope="row">{{$item->email}}</td>
                    <td scope="row">{{$item->phone}}</td>
                    <td scope="row">{{$item->address}}</td>
                    <td scope="row">{{$item->created_at}}</td>
                    <td scope="row">{{$item->updated_at}}</td>
                    <td scope="row">
                        <a href="{{route("student.delete",$item->id)}}"><button type="button" class="btn btn-danger">Delete</button></a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table> 
    </x-slot>       
    <x-slot name="footer">
    </x-slot>
</x-backend.card>
@include('backend.layouts.alert')      
@endsection