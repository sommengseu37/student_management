@php
    // dd($subjects)
@endphp
@extends('backend.layouts.app')

@section('title', __('Dashboard'))
@push('after-styles')
    <style>
        .card {
            background: #ffffff !important;
        }
    </style>
@endpush
@section('content')
    <x-backend.card>
        <x-slot name="header">
            @lang('Welcome :Name', ['name' => $logged_in_user->name])
        </x-slot>

        <x-slot name="body">
            @lang('')
            <table class="table table-bordered">
                <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Subject</th>
                    </tr>
                @foreach ($subjects  as $item)
                    <tr>
                        <th scope="row">{{$item->id}}</th>
                        <td>{{$item->subject}}</td>
                    </tr>
                </thead>
                 @endforeach
            </table>
        </x-slot>
        <x-slot name="footer">
            footer
        </x-slot>
    </x-backend.card>
@endsection
