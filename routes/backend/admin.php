<?php

use App\Http\Controllers\Backend\DashboardController;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use Tabuna\Breadcrumbs\Trail;

// All route names are prefixed with 'admin.'.
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])
    ->name('dashboard')
    ->breadcrumbs(function (Trail $trail) {
        $trail->push(__('Home'), route('admin.dashboard'));
    });
Route::get('class',[DashboardController::class, 'class'])
    ->name('class')
    ->breadcrumbs(function (Trail $trail){
        $trail->push(__('Home'), route('admin.class'));
    });
Route::get('subject',[DashboardController::class, 'subject'])
    ->name('subject')
    ->breadcrumbs(function (Trail $trail){
        $trail->push(__('Home'),route('admin.subject'));
    });
Route::get('group', [DashboardController::class, 'group'])
    ->name('group')
    ->breadcrumbs(function (Trail $trail){
        $trail->push(__('Home'),route('admin.group'));
    });
Route::get('student', [DashboardController::class, 'student'])
    ->name('student')
    ->breadcrumbs(function (Trail $trail){
        $trail->push(__('Home'),route('admin.student'));
    });

Route::get('create',[DashboardController::class, 'create'])
    ->name('create')
    ->breadcrumbs(function (Trail $trail){
        $trail->push(__('Home'),route('admin.create'));
    });