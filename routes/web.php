<?php

use App\Http\Controllers\ClassroomController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\LocaleController;
use App\Models\Classroom;
/*
 * Global Routes
 *
 * Routes that are used between both frontend and backend.
 */

// Switch between the included languages
Route::get('lang/{lang}', [LocaleController::class, 'change'])->name('locale.change');

/*
 * Frontend Routes
 */
Route::group(['as' => 'frontend.'], function () {
    includeRouteFiles(__DIR__.'/frontend/');
});

/*
 * Backend Routes
 *
 * These routes can only be accessed by users with type `admin`
 */
Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => 'admin'], function () {
    includeRouteFiles(__DIR__.'/backend/');
});

// Route::get('/admin/class/add', [UserController::class, 'index'])->name('/add/class');
Route::get('/classroom', [ClassroomController::class, 'index'])->name("classroom.index");
Route::get('/classroom/create', [ClassroomController::class, 'create'])->name("classroom.create");
Route::post('/classroom/store', [ClassroomController::class, 'store'])->name('classroom.store');

Route::get('/classroom/{id}/show', [ClassroomController::class, 'show'])->name("classroom.show");
Route::get('/classroom/{id}/edit', [ClassroomController::class, 'edit'])->name("classroom.edit");
Route::post('/classroom/{id}/update', [ClassroomController::class, 'update'])->name("classroom.update");
Route::get('/classroom/{id}/delete', [ClassroomController::class, 'delete'])->name("classroom.delete");

Route::get('/admin/student', [StudentController::class, 'index'])->name("student.all");
Route::get('/admin/student/create', [StudentController::class, 'create'])->name("student.create");

Route::post('/student/store', [StudentController::class, 'store']);

// Route::get('/student/show', [StudentController::class, 'show'])->name("student.show");

Route::get('/student/{id}/delete', [StudentController::class, 'delete'])->name("student.delete");
